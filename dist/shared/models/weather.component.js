"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var weather_service_1 = require("../../services/weather.service");
var WeatherComponent = (function () {
    function WeatherComponent(_weather) {
        var _this = this;
        this._weather = _weather;
        setInterval(function () {
            _this.getCurrentAndFiveDayWeather();
        }, 1800000);
    }
    WeatherComponent.prototype.ngOnInit = function () {
        this.getCurrentAndFiveDayWeather();
    };
    WeatherComponent.prototype.convertTemp = function (temp) {
        return Math.ceil(temp);
    };
    WeatherComponent.prototype.convertUnixToIst = function (time) {
        var a = new Date(time * 1000);
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var hours = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = hours[a.getHours()];
        var min = a.getMinutes();
        var mins;
        if (min <= 9) {
            mins = "0" + min;
        }
        else {
            mins = min;
        }
        var sec = a.getSeconds();
        var day = days[a.getDay()];
        var times = day + ', ' + date + ' ' + month + ' ' + year + ' ' + hour + ':' + mins;
        return times;
    };
    WeatherComponent.prototype.getCurrentAndFiveDayWeather = function () {
        var _this = this;
        this._weather.getCurrentAndFiveDayWeather().subscribe(function (data) {
            _this.current = data[0];
            _this.fiveDays = data[1];
        });
    };
    return WeatherComponent;
}());
WeatherComponent = __decorate([
    core_1.Component({
        selector: 'weather-app',
        templateUrl: './app/shared/models/weather.component.html',
        styleUrls: ['./app/app.component.css']
    }),
    __metadata("design:paramtypes", [weather_service_1.WeatherService])
], WeatherComponent);
exports.WeatherComponent = WeatherComponent;
//# sourceMappingURL=weather.component.js.map