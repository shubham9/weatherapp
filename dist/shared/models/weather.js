"use strict";
var Weather = (function () {
    function Weather(_weather) {
        var _this = this;
        this._weather = _weather;
        setInterval(function () {
            _this.getCurrentAndFiveDayWeather();
        }, 1800000);
    }
    Weather.prototype.ngOnInit = function () {
        this.getCurrentAndFiveDayWeather();
    };
    Weather.prototype.convertTemp = function (temp) {
        return Math.ceil(temp);
    };
    Weather.prototype.convertUnixToIst = function (time) {
        var a = new Date(time * 1000);
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var hours = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = hours[a.getHours()];
        var min = a.getMinutes();
        var mins;
        if (min <= 9) {
            mins = "0" + min;
        }
        else {
            mins = min;
        }
        var sec = a.getSeconds();
        var day = days[a.getDay()];
        var times = day + ', ' + date + ' ' + month + ' ' + year + ' ' + hour + ':' + mins;
        return times;
    };
    Weather.prototype.getCurrentAndFiveDayWeather = function () {
        var _this = this;
        this._weather.getCurrentAndFiveDayWeather().subscribe(function (data) {
            _this.current = data[0];
            _this.fiveDays = data[1];
        });
    };
    return Weather;
}());
exports.Weather = Weather;
//# sourceMappingURL=weather.js.map