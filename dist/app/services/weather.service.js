"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
//Weather service to get data from OpenWeatherMapApi using observable
var WeatherService = (function () {
    function WeatherService(http) {
        this.http = http;
    }
    WeatherService.prototype.getCurrentAndFiveDayWeather = function () {
        return Rx_1.Observable.forkJoin(
        //this will get the current weather data
        this.http.get('http://api.openweathermap.org/data/2.5/weather?id=1269515&units=metric&APPID=d74162433243f0804f69cde6ee1189c6').map(function (res) { return res.json(); }), 
        //this will get the 5 day forecast data
        this.http.get('http://api.openweathermap.org/data/2.5/forecast?id=1269515&units=metric&APPID=d74162433243f0804f69cde6ee1189c6').map(function (res) { return res.json(); }), 
        //this will get the weather data for next 4 days
        this.http.get('http://api.openweathermap.org/data/2.5/forecast/daily?id=1269515&cnt=6&units=metric&APPID=d74162433243f0804f69cde6ee1189c6').map(function (res) { return res.json(); }));
    };
    return WeatherService;
}());
WeatherService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], WeatherService);
exports.WeatherService = WeatherService;
//# sourceMappingURL=weather.service.js.map