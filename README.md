# README #


WeatherApp to get the 5-day forecast on 3 hourly bases.
This application is written completely on Angular2 and Bootstrap3.

Version - 0.0.1


### How do I get set up? ###
1. Install all dependencies by running - 
|npm install|

2. Run Application - 
|npm start|

Future developments -

1.  Get weather for any city by mapping city name with city code as in accordance with ISO 3166 standard.

2.  Get Wiki extract using wiki API like(https://en.wikipedia.org/w/api.php?
format=json&action=query&prop=extracts&exintro=&explaintext=&titles=jaipur&redirects=1) 

3. Get temperature in Celsius/Fahrenheit

4. D3.js integration to show temperature variations over the time on a graph.

5. Test cases for components and service.

### Live Version ###
http://www.dash.work/weatherapp