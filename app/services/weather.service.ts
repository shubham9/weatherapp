import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
//Weather service to get data from OpenWeatherMapApi using observable
@Injectable()
export class WeatherService {

  constructor(private http:Http) { }
  getCurrentAndFiveDayWeather() {
    return Observable.forkJoin(
      //this will get the current weather data
      this.http.get('http://api.openweathermap.org/data/2.5/weather?id=1269515&units=metric&APPID=d74162433243f0804f69cde6ee1189c6').map((res:Response) => res.json()),
      //this will get the 5 day forecast data
      this.http.get('http://api.openweathermap.org/data/2.5/forecast?id=1269515&units=metric&APPID=d74162433243f0804f69cde6ee1189c6').map((res:Response) => res.json()),
      //this will get the weather data for next 4 days
      this.http.get('http://api.openweathermap.org/data/2.5/forecast/daily?id=1269515&cnt=6&units=metric&APPID=d74162433243f0804f69cde6ee1189c6').map((res:Response) => res.json())
    )
  }

}
