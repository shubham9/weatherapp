import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { WeatherComponent} from './weather/weather.component';
import { HttpModule } from '@angular/http';
import { WeatherService } from './services/weather.service';

@NgModule({
  imports:[BrowserModule,HttpModule],
  declarations:[AppComponent,WeatherComponent],
  providers:[WeatherService],
  bootstrap:[AppComponent]
})
export class AppModule {}
