import { Component } from '@angular/core';
import { WeatherComponent } from './weather/weather.component';
import {Observable} from 'rxjs/Rx';
//Root component of the app
@Component({
  selector:'dash',
  templateUrl:'./app/app.component.html',
  styleUrls:['./app/app.component.css']
})
export class AppComponent {
  constructor() {}
}
