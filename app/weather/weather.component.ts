import { Component } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { WeatherService } from '../services/weather.service'
//Main weather component that will consume the service that gets weather data from OpenWeatherMapApi.
@Component({
  selector:'weather-app',
  templateUrl:'./app/weather/weather.component.html',
  styleUrls:['./app/app.component.css']
})
export class WeatherComponent{
  public current;
  public fiveDays;
  public daily;
  activeDate;
  //Method to be invoked on initialisation to get the weather data from weather service
  ngOnInit() {
    this.getCurrentAndFiveDayWeather();
  }
  //Method to convert the temperature in decimal to give weather temperature value as integer
  convertTemp(temp){
    return Math.ceil(temp);
  }
  //Method to convert unix timestamp provided by OpenWeatherMapApi to readable date format
  convertUnixToIst(time:number): string {
      let a = new Date(time * 1000);
      let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
      let days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
      let hours = ['00','01','02','03','04','05','06','07','08','09','10','11','12','01','02','03','04','05','06','07','08','09','10','11']
      let year = a.getFullYear();
      let month = months[a.getMonth()];
      let date = a.getDate();
      let hour = hours[a.getHours()];
      let min = a.getMinutes();
      let mins;
      if(min<=9){
        mins = "0"+min;
      }
      else{
        mins=min;
      }
      let sec = a.getSeconds();
      let day = days[a.getDay()];
      let times = day + ', '+ date + ' ' + month + ' ' + year + '  ' + hour + ':' + mins ;
      return times;
  }
  constructor(private _weather: WeatherService) {
    setInterval(()=>{
      this.getCurrentAndFiveDayWeather();
    },1800000);
  }
  //Method which will be invoked on click event to return weather data against selected date
  showWeather(fiveDays,date){
    if(date<=9){
      date = "0"+date;
    }
    this.activeDate = fiveDays.filter((item) => (item.dt_txt.split(' ')[0].split('-')[2]==date));
    console.log(this.activeDate);
  }
//Method to consume data from weather service and set the public variables with data value
  getCurrentAndFiveDayWeather() {
    this._weather.getCurrentAndFiveDayWeather().subscribe(
      data => {
        this.current = data[0]
        this.fiveDays = data[1]
        this.daily = data[2]
      }
    )
  }

}
